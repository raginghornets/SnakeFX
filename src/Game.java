import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javafx.animation.AnimationTimer;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Game {
  private static int size = 20;
  private static int[][] grid = new int[25][25];
  private static int scr_size = grid.length * size;
  
  private static StackPane gameLayout = new StackPane();         
  private static Scene gameScene = new Scene(gameLayout, scr_size, scr_size);
  private static Canvas canvas = new Canvas(gameScene.getWidth(), gameScene.getHeight());
  private static GraphicsContext ctx = canvas.getGraphicsContext2D();
  
  public void start() {
    Stage game = new Stage();
    game.setTitle("Snake");
    game.setOnCloseRequest(req -> System.exit(0));
    
    // Setup game objects
    int snakeX = grid.length / 4;
    int snakeY = grid.length / 2;
    List<int[]> snake = new LinkedList<int[]>();
    snake.add(new int[] {snakeX, snakeY});
    snake.add(new int[] {snakeX-1, snakeY});
    snake.add(new int[] {snakeX-2, snakeY});
    int[] newHead = {snake.get(0)[0], snake.get(0)[1]};
    int[] snakeSpeed = {0, 0};
    int[] food = {grid.length / 2, grid.length / 2};
    
    // Draw background
    Color bgColor = Color.MEDIUMPURPLE;
    ctx.setFill(bgColor);
    ctx.fillRect(0, 0, scr_size, scr_size);
    
    // Snake controls
    gameScene.setOnKeyPressed(new EventHandler<KeyEvent>() {

      @Override
      public void handle(KeyEvent e) {
        boolean snakeGoingUP = snakeSpeed[1] == -1;
        boolean snakeGoingLEFT = snakeSpeed[0] == -1;
        boolean snakeGoingDOWN = snakeSpeed[1] == 1;
        boolean snakeGoingRIGHT = snakeSpeed[0] == 1;
        switch(e.getCode()) {
          case UP:
          case W:
            if(!snakeGoingDOWN) {
              snakeSpeed[0] = 0;
              snakeSpeed[1] = -1;
            }
            break;
          case LEFT:
          case A:
            if(!snakeGoingRIGHT) {
              snakeSpeed[0] = -1;
              snakeSpeed[1] = 0;
            }
            break;
          case DOWN:
          case S:
            if(!snakeGoingUP) {
              snakeSpeed[0] = 0;
              snakeSpeed[1] = 1;
            }
            break;
          case RIGHT:
          case D:
            if(!snakeGoingLEFT) {
              snakeSpeed[0] = 1;
              snakeSpeed[1] = 0;
            }
            break;
          default:
            break;
        }
      }
      
    });
    game.setScene(gameScene);
    
    gameLayout.getChildren().add(canvas);
    
    game.show();
    
    Runnable update = new Runnable() {
      
      long lastTime = System.nanoTime();
      
      @Override
      public void run() {
        
        // Control how fast snake game is
        long time = System.nanoTime();
        long interval = 28000000;
        
        if(time - lastTime > interval * 2.5f) {
          
          // New head add speed
          newHead[0] += snakeSpeed[0];
          newHead[1] += snakeSpeed[1];
          
          // No going back
          if(Arrays.equals(snake.get(1), newHead)) {
            newHead[0] = snake.get(0)[0];
            newHead[1] = snake.get(0)[1];
            snakeSpeed[0] *= -1;
            snakeSpeed[1] *= -1;
          }
          
          // Snake die
          if(newHead[0] < 0
              || newHead[0] > grid.length - 1
              || newHead[1] < 0
              || newHead[1] > grid.length - 1
              || pointsContains(snake.subList(1, snake.size()-1), (newHead))) {
            System.exit(0);
          }
          
          // Snake eat food
          if(Arrays.equals(newHead, food)) {
            
            // Snake grow
            snake.add(newHead);
            
            // Put food somewhere else
            Random random = new Random();
            while(pointsContains(snake, food)) {
              food[0] = random.nextInt(grid.length);
              food[1] = random.nextInt(grid.length);
            }
          }
          
          // If new head isn't in the snake
          if(!Arrays.equals(snake.get(0), newHead) && !Arrays.equals(snake.get(1), newHead)) {
            
            // Erase tail
            drawPoint(snake.get(snake.size()-1), bgColor);
          
            // Move to the new head
            snake.add(0, new int[] {newHead[0], newHead[1]});
            snake.remove(snake.size() - 1);
            
        // Head is in the snake
        } else {
          newHead[0] = snake.get(0)[0];
          newHead[1] = snake.get(0)[1];
        }

        lastTime = time;
        }
      }
    };
    
    // Draw everything
    AnimationTimer gameLoop = new AnimationTimer() {

      @Override
      public void handle(long now) {
        
        // Update game
        (new Thread(update)).start();
        
        // Draw food
        drawPoint(food, Color.DARKSALMON);
        
        // Draw snake
        snake.forEach(point -> drawPoint(point, Color.BEIGE));
      }
    
    };
    
    gameLoop.start();
  }
  
  private static void drawPoint(int[] point, Color color) {
    ctx.setFill(color);
    ctx.fillRect(point[0] * size, point[1] * size, size, size);
  }
  
  private static boolean pointsContains(List<int[]> points, int[] item) {
    if(points.get(0).length != 2) return false;
    for(int[] point : points) {
      if(point[0] == item[0] && point[1] == item[1]) return true; 
    }
    return false;
  }
}
